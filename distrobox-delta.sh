#!/bin/sh
list_dirs=("boot" "etc" "opt" "root" "srv" "usr" "var")
system_dirs=("proc" "sys" "dev" "run" "tmp")
dtb_delta="/var/lib/distrobox-delta"  #### need real file system, not overlayfs
dummy_image_name="dummy-image-distrobox"
function overlay_create_and_mount(){
    operation_dir="$dtb_delta/$box_name"
    sudo mkdir -p $operation_dir
        for i in ${list_dirs[@]} ; do
         sudo mkdir -p $operation_dir/.overlay/work-$i
         sudo mkdir -p $operation_dir/.overlay/upper-$i
         sudo mkdir -p $operation_dir/$i
         if ! mountpoint -q $operation_dir/$i > /dev/null; then
            sudo mount  -t overlay overlay -o noatime,lowerdir=/$i,upperdir=$operation_dir/.overlay/upper-$i,workdir=$operation_dir/.overlay/work-$i $operation_dir/$i
         fi
        done
}
function dummy_image_create(){
tarball_dir=$(mktemp -d -p /tmp)
   tar -cf $tarball_dir/$dummy_image_name.tar --no-recursion --files-from <(find / -mindepth 1 -maxdepth 1 -type d -or -type l)
    sudo podman image import $tarball_dir/$dummy_image_name.tar --message tag $dummy_image_name:1
}
function distrobox_invocation(){
    for i in ${list_dirs[@]} ; do
        volume_mounts="${volume_mounts[@]} --volume $dtb_delta/$box_name/$i:/$i"
    done
    distrobox create \
    $box_name \
    --root \
    --image localhost/$dummy_image_name:1 \
    --additional-flags "--cap-add=ALL" \
    ${volume_mounts[@]}

}
printf "###########################################################\n"
printf "                Distrobox Delta                               \n"
printf "###########################################################\n"
function main(){
if ! sudo podman container exists $box_name ; then
    overlay_create_and_mount
    dummy_image_create
    distrobox_invocation
    distrobox enter --root $box_name
else
    overlay_create_and_mount
    distrobox enter --root $box_name
fi
}
if [ ! -z $1 ] ; then
    box_name=$1
    main
else
    box_name="arch-vbox-delta"
    main
fi
